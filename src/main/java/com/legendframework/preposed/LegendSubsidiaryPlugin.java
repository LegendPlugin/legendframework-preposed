package com.legendframework.preposed;

import com.legendframework.core.AbstractLegendPlugin;
import com.legendframework.core.Store;
import org.bukkit.plugin.IllegalPluginAccessException;

/**
 * @author 196
 *
 * 插件主类的父类
 */
public abstract class LegendSubsidiaryPlugin extends AbstractLegendPlugin {

    @Override
    protected Store createStore() {
        return null;
    }

    /**
     * 是否有效
     */
    private boolean isUse;

    @Override
    public void onLoad() {
        if (!isUse) {
            throw new IllegalPluginAccessException("由legendframework-preposed框架开发的插件，不能直接放置在server/plugins 目录下");
        }
        load();
    }
}
