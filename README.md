# legendframework-preposed

#### 介绍
插件开发框架前置,如果你不想每次开发的插件最小容量都为400kb+，推荐使用legendframework-preposed前置依赖，

#### 安装教程
将legendframework-preposed放入 “核心所在目录/plugins" 目录下，初次加载会生成目录：“核心所在目录/plugins/Legendframework-Preposed/plugins/” ， 将所有依赖legendframework-preposed的插件放入生成的plugins目录中即可，插件会被legendframework-preposed所加载到服务端中

#### 使用说明
legendframework-preposed的使用与legendframework-core有些许不同（好吧其实只是父类不同而已）

1. 主类继承LegendSubsidiaryPlugin类

好了完了，就这么简单，其余使用教程请看 legendframework-core 